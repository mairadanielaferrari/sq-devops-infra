resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags {
    Name                = "${var.deployment-prefix}-igw"
    Organization        = "${var.organization}"
    Project             = "${var.project}"
    Environment         = "${var.environment}"
  } 
}