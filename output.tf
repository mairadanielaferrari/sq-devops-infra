###################################################################################################
# Vpc and subnets
###################################################################################################
output "vpc-id" {
  value = "${aws_vpc.vpc.id}"
}

output "subnets" {
  value = {
    "public" = "${aws_subnet.public-subnet.*.id}"
    "private" = "${aws_subnet.private-subnet.*.id}"
  }
}
###################################################################################################

###################################################################################################
# Security Groups
###################################################################################################
output "web-server-sg-id" {
  value = "${aws_security_group.web-server-sg.id}"
}

output "sql-server-sg-id" {
  value = "${aws_security_group.sql-server-sg.id}"
}

output "ec2-role-name" {
  value = "${aws_iam_role.ec2-role.name}"
}

output "ec2-instance-profile-name" {
  value = "${aws_iam_instance_profile.ec2-instance-profile.name}"
}
###################################################################################################

###################################################################################################
# Redis Server
###################################################################################################
# output "redis_endpoint_address" {
#   value = "${aws_elasticache_replication_group.redis-replication-group.primary_endpoint_address}"
# }

output "redis-server-address" {
  value = "${aws_instance.redis-server.private_dns}"
}

output "redis-server-port" {
  value = "${var.redis-cache-port}"
}

output "redis-server-password" {
  value = "${random_string.redis-auth-token.result}"
}
###################################################################################################