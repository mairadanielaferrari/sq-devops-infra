#!/bin/bash

export PATH=$PATH:/opt/terraform &&
terraform init &&

# Argument/Env Variable Verification
if [ \( -z "$AWS_ACCESS_KEY_ID" \) -a \( $# -ne 4 \) ]
  then
    echo -e "Invalid number of arguments/environment variables \n
		1) AWS_ACCESS_KEY_ID \n
		2) AWS_SECRET_ACCESS_KEY \n
		3) AWS_DEFAULT_REGION \n
		4) BUCKET_NAME \n
	  Example: ./install.sh ${AWS_ACCESS_KEY_ID} ${AWS_SECRET_ACCESS_KEY} ${AWS_DEFATUL_REGION} ${BUCKET_NAME} \n"
    exit
elif [ $# -eq 4 ]
	then
		export AWS_ACCESS_KEY_ID=$1
		export AWS_SECRET_ACCESS_KEY=$2
		export AWS_DEFAULT_REGION=$3
		export BUCKET_NAME=$4
fi

# Download settings and terraform state for existing deployment - may fail for new deployment
aws s3 cp s3://$BUCKET_NAME/settings.json . &&
aws s3 cp s3://$BUCKET_NAME/infra/terraform.tfstate .

echo "Running Terraform templates..."
terraform apply \
  -var "aws-access-key=$AWS_ACCESS_KEY_ID" \
  -var "aws-secret-key=$AWS_SECRET_ACCESS_KEY" \
  -var "aws-region=$AWS_DEFAULT_REGION" \
  -var "s3-bucket-name=$BUCKET_NAME" \
  -var-file="settings.json" \
  -auto-approve &&

# echo "Getting redis cache endpoint..."
# DEPLOYMENT_PREFIX=$(cat settings.json | jq -r '.["deployment-prefix"]' ) &&

# echo "Deployment prefix: $DEPLOYMENT_PREFIX"
# aws elasticache describe-cache-clusters --cache-cluster-id "${DEPLOYMENT_PREFIX}-rc" --show-cache-node-info > aws-redis.json &&
# REDIS_ENDPOINT=$(cat aws-redis.json | jq '.CacheClusters[].CacheNodes[].Endpoint') &&
# echo $REDIS_ENDPOINT > aws-redis.json &&

# echo "Redis Endpoint: $REDIS_ENDPOINT"
# echo "Terraform Apply was successfully executed... Generating settings file with output variables"

# tf-output outputs -p . -f json > infra-settings.json &&
# jq -s '.[0] * {"redis-endpoint": { port:.[1].Port, address:.[1].Address}}' infra-settings.json aws-redis.json > infra-redis-settings.json &&
# jq -s '.[0] * .[1]' settings.json infra-redis-settings.json > settings-merged.json &&

# Updating settings
tf-output outputs -p . -f json > infra-settings.json &&
jq -s '.[0] * .[1]' settings.json infra-settings.json > settings-merged.json &&

echo "Uploading setting and terraform state to ${BUCKET_NAME}"
aws s3 cp settings-merged.json s3://$BUCKET_NAME/settings.json &&
aws s3 cp ./terraform.tfstate s3://$BUCKET_NAME/infra/terraform.tfstate &&

echo "settings.json file was successfully uploaded to s3://$BUCKET_NAME bucket"
rm -rf aws-redis.json &&
rm -rf settings.json &&
rm -rf infra-settings.json &&
# rm -rf infra-redis-settings.json &&
rm -rf settings-merged.jsons

