resource "aws_security_group" "web-server-sg" {
  vpc_id      = "${aws_vpc.vpc.id}"
  name        = "${var.deployment-prefix}-web-server-sg"
  description = "Allow web inbound traffic"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 
   ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  # Redis
  ingress {
    from_port   = "${var.redis-cache-port}"
    to_port     = "${var.redis-cache-port}"
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc-cidr-block}"]
  }
  
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
  tags {
    Name = "${var.deployment-prefix}-web-server-sg"
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  }
}

resource "aws_security_group" "sql-server-sg" {
  vpc_id      = "${aws_vpc.vpc.id}"
  name        = "${var.deployment-prefix}-sql-server-sg"
  description = "Allow sql inbound traffic"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc-cidr-block}"]
  }

   ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc-cidr-block}"]
  }
  
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
  tags {
    Name = "${var.deployment-prefix}-sql-server-sg"
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  }
}

resource "aws_security_group" "redis-server-sg" {
  vpc_id      = "${aws_vpc.vpc.id}"
  name        = "${var.deployment-prefix}-redis-server-sg"
  description = "Redis in-vpc access"

  # Redis
  ingress {
    from_port   = "${var.redis-cache-port}"
    to_port     = "${var.redis-cache-port}"
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc-cidr-block}"]
  }

  # Ssh
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.vpc-cidr-block}"]
  }
  
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
  tags {
    Name = "${var.deployment-prefix}-redis-server-sg"
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  }
}