#http://blog.kaliloudiaby.com/index.php/terraform-to-provision-vpc-on-aws-amazon-web-services/
resource "aws_eip" "igw-eip" {
  vpc      = true
  depends_on = ["aws_internet_gateway.igw"]
  tags {
    Name                = "${var.deployment-prefix}-igw-eip"
    Organization        = "${var.organization}"
    Project             = "${var.project}"
    Environment         = "${var.environment}"
  }  
}

resource "aws_nat_gateway" "natgw" {
  allocation_id = "${aws_eip.igw-eip.id}"
  subnet_id = "${aws_subnet.public-subnet.0.id}"
  depends_on = ["aws_internet_gateway.igw"]

  tags {
    Name                = "${var.deployment-prefix}-natgw"
    Organization        = "${var.organization}"
    Project             = "${var.project}"
    Environment         = "${var.environment}"
  }
}

resource "aws_route_table" "private-rt" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.natgw.id}"
  }
  tags {
    Name                = "${var.deployment-prefix}-private-route-table"
    Organization        = "${var.organization}"
    Project             = "${var.project}"
    Environment         = "${var.environment}"
  } 
}

resource "aws_route_table_association" "pr_sn_rt_association" {
  count           = "${local.azCount}"
  subnet_id       = "${element(aws_subnet.private-subnet.*.id, count.index)}"
  route_table_id  = "${aws_route_table.private-rt.id}"
}