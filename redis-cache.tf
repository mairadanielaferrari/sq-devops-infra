locals {
  private-subnets        = "${aws_subnet.private-subnet.*.id}"
}

resource "random_string" "redis-auth-token" {
  length = 32
  special = false
}

data "template_file" "redis-user-data" {
  template = "${file("redis-user-data.sh")}"
  vars {
    redis-auth-token         = "${random_string.redis-auth-token.result}"
  }
}

resource "aws_instance" "redis-server" {
  count                         = "1"
  ami                           = "${var.redis-server-ami-id}"
  instance_type                 = "${var.redis-server-instance-type}"
  key_name                      = "${var.redis-server-key-pair}"
  subnet_id                     = "${element(local.private-subnets, 0)}"
  associate_public_ip_address   = false
  security_groups               = ["${aws_security_group.redis-server-sg.id}"]
  user_data                     = "${data.template_file.redis-user-data.rendered}"
  
  tags {
    Name            = "${var.deployment-prefix}-redis-server"
    Organization    = "${var.organization}"
    Project         = "${var.project}"
    Environment     = "${var.environment}"
  }
}

# Source: https://github.com/azavea/terraform-aws-redis-elasticache/blob/develop/variables.tf

# resource "aws_elasticache_subnet_group" "redis-subnet-group" {
#  name       = "${var.deployment-prefix}-cache-subnet"
#  subnet_ids = ["${aws_subnet.private-subnet.*.id}"]
# }
  
# resource "aws_elasticache_replication_group" "redis-replication-group" {
#   replication_group_id          = "${var.deployment-prefix}-redis-rg"
#   replication_group_description = "${var.deployment-prefix} redis replication group"
#   engine                        = "redis"
#   engine_version                = "5.0.0"
#   parameter_group_name          = "default.redis5.0"
#   automatic_failover_enabled    = false
# #   availability_zones            = ["${local.awsZones}"]
#   node_type                     = "${var.redis-cache-instance-type}"
#   number_cache_clusters         = "${var.redis-cache-clusters-count}"
#   port                          = "${var.redis-cache-port}"
# #   transit_encryption_enabled    = true
# #   auth_token                    = "${random_string.redis-auth-token.result}"
#   subnet_group_name             = "${aws_elasticache_subnet_group.redis-subnet-group.name}"
#   security_group_ids            = ["${aws_security_group.web-server-sg.id}"]  
  
# #   cluster_mode {
# #     replicas_per_node_group     = 0
# #     num_node_groups             = 1
# #   }
  
#   tags {
#    Name                = "${var.deployment-prefix}-redis-replication-group"
#    Organization        = "${var.organization}"
#    Project             = "${var.project}"
#    Environment         = "${var.environment}"
#   }
# }

# resource "aws_elasticache_cluster" "redis-cache" {
#   cluster_id                    = "${var.deployment-prefix}-rc"
#   engine                        = "redis"
#   node_type                     = "${var.redis-cache-instance-type}"
#   num_cache_nodes               = "${var.redis-cache-instance-count}"
#   port                          = "${var.redis-cache-port}"
#   subnet_group_name             = "${aws_elasticache_subnet_group.redis-subnet-group.name}"
#   security_group_ids            = ["${aws_security_group.web-server-sg.id}"]

#   tags {
#    Name                = "${var.deployment-prefix}-redis-cache"
#    Organization        = "${var.organization}"
#    Project             = "${var.project}"
#    Environment         = "${var.environment}"
#   }
# }

# resource "aws_elasticache_cluster" "redis-cache" {
#   # (resource arguments)
# }
