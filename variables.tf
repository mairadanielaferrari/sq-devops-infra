# Creds
variable "aws-access-key" {}
variable "aws-secret-key" {}

# Region
variable "aws-region" {
  default = ""
}

# Subnets
variable "aws-azs" {
  type = "map"
  default = {
    "us-east-2" = "us-east-2a,us-east-2b"
  }
}

# Naming and tagging
variable "deployment-prefix" {
  default = ""
}

variable "organization" {
  default = ""
}
variable "project" {
  default = "n"
}
variable "environment" {
  default = ""
}

# IP Addresses to suffice for 4 subnets, ~254 addresses each
variable "vpc-cidr-block" {
  default = "10.0.0.0/22"
}

# Redis Cache
variable "redis-server-ami-id" { default = "ami-9c0638f9" }
variable "redis-server-instance-type" { default = "t2.micro" }
variable "redis-server-key-pair" { default = "vkhazin" }

variable "redis-cache-instance-type" {
  default = "cache.t2.micro"
}

variable "redis-cache-clusters-count" {
  default = "1"
}

variable "redis-cache-instance-count" {
  default = "1"
}

variable "redis-cache-parameter-group-name" {
  default = "default.redis5.0"
}

variable "redis-cache-port" {
  default = "6379"
}

# s3-bucket-name
variable "s3-bucket-name" {
  default = ""
}
