#!/bin/bash
redis_install_path="/opt/redis-3.2.7"
sudo sudo yum install epel-release -y &&
#sudo yum install redis -y &&
sudo yum install wget make gcc tcl
sudo wget http://download.redis.io/releases/redis-3.2.7.tar.gz
sudo tar -xvzf redis-3.2.7.tar.gz -C /opt
sudo rm -rf redis-3.2.7.tar.gz
cd $redis_install_path
cd deps
sudo make hiredis lua jemalloc linenoise
sudo make geohash-int
cd ../
sudo make
sudo make install
sudo sed 's/bind 127.0.0.1/# bind 127.0.0.1/g' $redis_install_path/redis.conf | tee $redis_install_path/redis.bind.conf &&
sudo sed 's/# requirepass foobared/requirepass ${redis-auth-token}/g' $redis_install_path/redis.bind.conf | tee $redis_install_path/redis.password.conf &&
sudo mv $redis_install_path/redis.conf $redis_install_path/redis.conf.bak &&
sudo mv $redis_install_path/redis.password.conf $redis_install_path/redis.conf &&
sudo rm $redis_install_path/redis.bind.conf &&
sudo systemctl enable redis &&
sudo systemctl start redis.service
